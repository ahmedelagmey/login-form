<? php
if (isset($_post['signupsubmit'])){
    require 'dbh.in.php';
    //data input
    $username    = $_post['usernm'];
    $email       = $_post['mail'];
    $passcode    = $_post['pswd1'];
    $passcodecon = $_post['pswd2'];
    if (empty($username) || empty($email) || empty($passcode) || empty($passcodecon)){
      header("location = ../index.php?error=emptyfalids".$username."&email=".$email);
      exit();
    }
    else if (!filter_var($email,FILTER_VALIDATE_EMAIL() || !preg_match("/[a-zA-Z0-9]*$/")) {
      header("location = ../index.php?error=invalidmail&usernsme=".$username);
      exit();
    }
    else if (!preg_match("/^[a-zA-Z0-9]*$/",$username)){
      header("location = ../index.php?error=invalidusername&amil=".$email);
      exit();
    }
    else if ($passcode !== $passcodecon){
      header("location = ../index.php?error=passcodecheck=".$username.$email);
      exit();
    }
    else {
      //data base statements
    }
}else {
 header("location:../index.php");
 exit;
}
