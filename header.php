<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="description" content="login" />
    <meta name="viewport" content="width=device-width, initial-scale=1 "/>
    <title> log in </title>
    <link rel="stylesheet" href="style/style.css"/>
  </head>
  <body>
    <!--header -->
    <header>
      <!--nav bar -->
      <nav>
        <ul>
          <li><a href="index.php">home</a></li>
          <li><a href="#"> <a>portofoilo</a></li>
          <li><a href="#" id="about">about me</a></li>
        </ul>
      </nav>
      <!-- log in -->
      <div class="header-login">
        <form class="login-f" action="includes/login.inc.php" method="post">
          <input type="text" name="userin" placeholder="user name" />
          <input type="password" name="pasw" placeholder="password" />
          <button type="submint" name="login-submit">login</button>
        </form>
        <!--register -->
        <a href="signup.php" class="signup-1">signup</a>
        <!--singout  -->
        <form class="sign-out-f" action="includes/logout.inc.php" method="post">
          <button type="submit" name="logout-submit">logout</button>
        </form>
      </div>
   </header>
    <!-- end of header -->
